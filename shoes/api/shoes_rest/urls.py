from django.urls import path
from .views import api_shoes_list, api_shoes_detail

urlpatterns = [
    path("shoes/", api_shoes_list, name="api_shoes_list"),
    path("shoes/<int:id>/", api_shoes_detail, name="api_shoes_detail"),
]
