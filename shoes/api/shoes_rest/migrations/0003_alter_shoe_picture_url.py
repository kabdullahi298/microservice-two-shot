# Generated by Django 4.0.3 on 2023-03-02 22:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0002_rename_shoes_shoe'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shoe',
            name='picture_url',
            field=models.URLField(null=True),
        ),
    ]
