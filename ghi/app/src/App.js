import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoeList'
import HatsList from './HatList';
import HatForm from './HatForm';
import NewShoeForm from './ShoeForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="hatform" element={<HatForm />} />
          <Route path="shoeform" element= {<NewShoeForm />} />
          <Route path="shoes" element={<ShoesList />} />
          <Route path="hats"element={<HatsList/>} />
          <Route path="/" element={<MainPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
