import { useEffect, useState } from 'react';

function HatsList() {
  const [hats, setHats] = useState([]);

  const getHats = async () => {
    const response = await fetch('http://localhost:8090/api/hats/');

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  };

  useEffect(() => {
    getHats();
  }, []);

  const handleDelete = async (id) => {
    const response = await fetch(`http://localhost:8090/api/hats/${id}/`, {
      method: 'DELETE',
    });

    if (response.ok) {
      getHats();
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Id</th>
          <th>Fabric</th>
          <th>Style Name</th>
          <th>Color</th>
          <th>Picture URL</th>
          <th>Location</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {hats.map((hat) => {
          return (
            <tr key={hat.id}>
              <td>{hat.id}</td>
              <td>{hat.fabric}</td>
              <td>{hat.style_name}</td>
              <td>{hat.color}</td>
              <td>{hat.picture_url}</td>
              <td>{hat.location}</td>
              <td>
                <button className="btn btn-danger" onClick={() => handleDelete(hat.id)}>
                  Delete
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatsList;
