import { useEffect, useState } from 'react';

function ShoesList() {
  const [shoes, setShoes] = useState([]);

  const getShoes = async () => {
    const response = await fetch('http://localhost:8080/api/shoes/');

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    }
  };

  useEffect(() => {
    getShoes();
  }, []);

  const handleDelete = async (id) => {
    const response = await fetch(`http://localhost:8080/api/shoes/${id}/`, {
      method: 'DELETE',
    });

    if (response.ok) {
      getShoes();
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Id</th>
          <th>Manufacturer</th>
          <th>Model Name</th>
          <th>Color</th>
          <th>Picture URL</th>
          <th>Bin</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map((shoe) => {
          return (
            <tr key={shoe.id}>
              <td>{shoe.id}</td>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.model_name}</td>
              <td>{shoe.color}</td>
              <td>{shoe.picture_url}</td>
              <td>{shoe.bin}</td>
              <td>
                <button className="btn btn-danger" onClick={() => handleDelete(shoe.id)}>
                  Delete
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ShoesList;
