import React, { useState, useEffect } from 'react';

function NewShoeForm() {
  const [bins, setBins] = useState([]);
  const [formData, setFormData] = useState({
    manufacturer: '',
    model_name: '',
    color: '',
    picture_url: '',
    bin: ''
  });

  const getBins = async () => {
    const response = await fetch('http://localhost:8100/api/bins/');
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }

  useEffect(() => {
    getBins();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch('http://localhost:8080/api/shoes/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData)
    });
    if (response.ok) {
      setFormData({
        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        bin: ''
      });
    }
  }

  const handleFormChange = (event) => {
    const inputName = event.target.name;
    const value = event.target.value;
    console.log("this is inputname/value", inputName, value)
    setFormData({
      ...formData,
      [inputName]: value
    });
  }
  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="manufacturer">Manufacturer:</label>
        <input
          type="text"
          className="form-control"
          id="manufacturer"
          name="manufacturer"
          value={formData.manufacturer}
          onChange={handleFormChange}
        />
      </div>
      <div className="form-group">
        <label htmlFor="model_name">Model Name:</label>
        <input
          type="text"
          className="form-control"
          id="model_name"
          name="model_name"
          value={formData.model_name}
          onChange={handleFormChange}
        />
      </div>
      <div className="form-group">
        <label htmlFor="color">Color:</label>
        <input
          type="text"
          className="form-control"
          id="color"
          name="color"
          value={formData.color}
          onChange={handleFormChange}
        />
      </div>
      <div className="form-group">
        <label htmlFor="picture_url">Picture URL:</label>
        <input
          type="text"
          className="form-control"
          id="picture_url"
          name="picture_url"
          value={formData.picture_url}
          onChange={handleFormChange}
        />
      </div>
      <div className="form-group">
        <label htmlFor="bin">Bin:</label>
        <select
          className="form-control"
          id="bin"
          required name="bin"
          value={formData.bin}
          onChange={handleFormChange}
        >
          <option value="">Select bin...</option>
          {bins.map(bin => {
            return (
            <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
            )
        })}
        </select>
      </div>
      <button type="submit" className="btn btn-primary">Create</button>
    </form>
  );
}

export default NewShoeForm;
