from django.urls import path
from .views import api_hatslist, api_hatsdetail


urlpatterns = [
    path("hats/", api_hatslist, name="api_hats_list"),
    path("hats/<int:pk>/", api_hatsdetail, name="api_hats_detail"),
]
