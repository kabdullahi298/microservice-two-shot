from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hat, LocationVO

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name"]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style_name", "color", "picture_url", "location"]
    encoders = {"location": LocationVODetailEncoder()}

@require_http_methods(["GET", "POST"])
def api_hatslist(request):

    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
            safe=False,
        )

    else:  # request.method == "POST"
        content = json.loads(request.body)
        location_href = content["location"]

        try:
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_hatsdetail(request, pk):

    try:
        hat = Hat.objects.get(id=pk)
    except Hat.DoesNotExist:
        return JsonResponse({"message": "This does not exist"}, status=404)

    if request.method == "GET":
        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        hat.delete()
        return JsonResponse(
            {"message": "Hat was deleted successfully! You're Welcome"},
            status=204,
        )

    else:  # request.method == "PUT"
        content = json.loads(request.body)
        location_href = content.get("location_href")

        if location_href:
            try:
                location = LocationVO.objects.get(import_href=location_href)
                content["location"] = location
            except LocationVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid Location id"},
                    status=400,
                )

        Hat.objects.filter(id=pk).update(**content)
        hat.refresh_from_db()

        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
            safe=False,
        )
